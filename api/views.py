from rest_framework import generics

from selection.models import Product
from .serializers import ProductsSerializer


class ProductApiView(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductsSerializer
