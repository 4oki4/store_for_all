from django.urls import path, include
from .views import ProductApiView

urlpatterns = [
    path('', ProductApiView.as_view()),
]