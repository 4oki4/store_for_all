from rest_framework import serializers
from selection.models import Product


class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('image', 'name', 'description', 'price', )
