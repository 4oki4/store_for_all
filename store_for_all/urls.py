"""store_for_all URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.views import get_swagger_view
from rest_framework.documentation import include_docs_urls


from django.views.generic.base import TemplateView
from django.conf import settings


#schema_view = get_schema_view(title="Store_for_all")
schema_view = get_swagger_view(title="Store_for_all")


urlpatterns = [
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
    path('admin/', admin.site.urls),
    path('users/', include('users.urls')),
    path('users/', include('django.contrib.auth.urls')),
    path('basket/', include('basket.urls', namespace='basket')),
    path('selection/', include('selection.urls', namespace='selection')),
    path('orders', include('orders.urls', namespace='orders')),
    path('api/', include('api.urls')),
    #path('schema/', schema_view),
    path('docs/', include_docs_urls(title="Store_for_all")),
    path('swagger-docs/', schema_view),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)




