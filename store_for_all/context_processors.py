from basket.views import get_basket
from decimal import Decimal


def basket(request):
    basket = get_basket(request)
    basket_total_price = sum(Decimal(item['price']) * item['quantity'] for item in basket.values())
    return {'basket_total_price': basket_total_price}
