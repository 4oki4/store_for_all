from django.shortcuts import render

from decimal import Decimal

from .models import OrderItems, Product
from .forms import OrderCreateForm
from basket.views import get_basket, basket_clear


def order_create(request):
    basket = get_basket(request)

    basket_qty = sum(item['quantity'] for item in basket.values())
    transport_cost = round((0.99 + basket_qty // 10), 2)

    if request.method == 'POST':
        order_form = OrderCreateForm(request.POST)
        if order_form.is_valid():
            cf = order_form.cleaned_data
            transport = cf['transport']
            if transport == 'Recipient pickup':
                transport_cost = 0

            order = order_form.save(commit=False)

            if request.user.is_authenticated:
                order.user = request.user

            order.transport_cost = Decimal(transport_cost)
            order.save()

            product_ids = basket.keys()
            products = Product.objects.filter(id__in=product_ids)

            for product in products:
                basket_item = basket[str(product.id)]
                OrderItems.objects.create(
                    order=order,
                    product=product,
                    price=basket_item['price'],
                    quantity=basket_item['quantity']
                )
                basket_clear(request)

                return render(request,
                              'orders/order_created.html',
                              {'order': order}
                               )
    else:
        order_form = OrderCreateForm()

    return render(request,
                  'orders/order_create.html',
                  {'basket': basket,
                   'order_form': order_form,
                   'transport_cost': transport_cost}
                  )