from django.shortcuts import render
from django.shortcuts import get_object_or_404, redirect
from .forms import ReviewForm

from basket.forms import BasketAddProductForm

from .models import Category, Product, Review


categories = Category.objects.all()


def product_list(request, category_slug=None):
    if category_slug:
        request_category = get_object_or_404(Category, slug=category_slug)
        products = Product.objects.filter(category=request_category)
    else:
        request_category = None
        products = Product.objects.all()

    return render(request, 'product/list.html',
                  {'categories': categories,
                   'request_category': request_category,
                   'products': products})


def product_detail(request, category_slug, product_slug):
    category = get_object_or_404(Category, slug=category_slug)
    product = get_object_or_404(Product, category_id=category.id, slug=product_slug)

    if request.method == 'POST':
        review_form = ReviewForm(request.POST)

        if review_form.is_valid():
            cf = review_form.cleaned_data
            author_name = 'Anonymous'
            Review.objects.create(product=product, author=author_name, text=cf['text'], rating=cf['rating'])

        return redirect('selection:product_detail', category_slug=category_slug, product_slug=product_slug)
    else:
        review_form = ReviewForm()
        basket_product_form = BasketAddProductForm()

    return render(request, 'product/detail.html',
                  {'product': product, 'review_form': review_form, 'basket_product_form': basket_product_form})
