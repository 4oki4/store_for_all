from django.shortcuts import render
from django.conf import settings
from .forms import BasketAddProductForm

from decimal import Decimal

from selection.models import Product

from django.shortcuts import get_object_or_404, redirect


def get_basket(request):
    basket = request.session.get(settings.BASKET_ID)
    if not basket:
        basket = request.session[settings.BASKET_ID] = {}
    return basket


def basket_add(request, product_id):
    basket = get_basket(request)

    product = get_object_or_404(Product, id=product_id)
    product_id = product.id
    form = BasketAddProductForm(request.POST)

    if form.is_valid():
        cd = form.cleaned_data
        if product_id not in basket:
            basket[product_id] = {'quantity': 0, 'price': str(product.price)}

        if request.POST.get('overwrite_qty'):
            basket[product_id]['quantity'] = cd['quantity']
        else:
            basket[product_id]['quantity'] += cd['quantity']

        request.session.modified = True

    return redirect('basket:basket_detail')


def basket_detail(request):
    basket = get_basket(request)
    product_ids = basket.keys()
    products = Product.objects.filter(id__in=product_ids)
    temp_basket = basket.copy()
    basket_total_price = 0

    for product in products:
        basket_item = temp_basket[str(product.id)]
        basket_item['product'] = product
        basket_item['total_price'] = (Decimal(basket_item['price']) * basket_item['quantity'])
        basket_total_price = sum(Decimal(item['price']) * item['quantity'] for item in temp_basket.values())

        basket_item['update_quantity_form'] = BasketAddProductForm(initial={'quantity': basket_item['quantity']})

    return render(request, 'basket/detail.html', {
        'basket': temp_basket.values(),
        'basket_total_price': basket_total_price
    })


def basket_remove(request, product_id):
    basket = get_basket(request)
    product_id = str(product_id)
    if product_id in basket:
        del basket[product_id]
        request.session.modified = True
    return redirect('basket:basket_detail')


def basket_clear(request):
    del request.session[settings.BASKET_ID]
